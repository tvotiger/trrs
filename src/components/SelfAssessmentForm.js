import React, { useState } from 'react';
import axios from 'axios';
import { Form, Button } from 'semantic-ui-react';
import _ from 'lodash';

const selfAssessmentFields = [
  { name : 'technical', label : 'Technical Risk' },
  { name : 'projectManagement', label : 'Project ManagementRisk' },
  { name : 'financialManagement', label : 'Financial Management Risk' },
  { name : 'scopeManagement', label : 'Scope Management Risk' },
  { name : 'resources', label : 'Resources Risk' },
  { name : 'customerSatisfaction', label : 'Customer Satisfaction Risk' },
  { name : 'security', label : 'Security Risk' },
];

export default function SelfAssessmentForm( props ) {

  const { projectId } = props;

  const [ postData, setPostData ] = useState({ project : Number.parseInt( projectId ) });
  const [ busy, setBusy ] = useState(false);
  const [ filelist, setFilelist ] = useState(null);
  
  const onSubmitClicked = async () => {
    setBusy(true);
    selfAssessmentFields.map( field => {
      setPostData(
        _.assign(
          postData,
          { [field.name] : document.querySelector(`input[name="${field.name}"]:checked`).value }
        )
      );
      return null;
    } );

    const result = await axios.post(
      `http://108.61.23.159/self-assessments`,
      postData,
      { headers : { 'Content-Type': 'application/json' } }
    )
    console.log( 'result', result );
    if ( result.status !== 200 ) {
      alert( 'Failed' );
      setBusy(false);
      return;
    } else {
      alert( 'Success' )
    }

    // FILE UPLOAD
    if ( !filelist ) {
      setBusy(false);
      return;
    }

    const selfAssessmentId = result.data.id;

    const formData = new FormData();
    formData.append( 'files', filelist[ 0 ] );
    formData.set( 'ref', 'self-assessment' );
    formData.set( 'refId', selfAssessmentId );
    formData.set( 'field', 'attachments' );

    const uploadResult = await axios.post(
      `http://108.61.23.159/upload`,
      formData,
      { headers : { 'Content-Type': 'multipart/form-data' } }
    )
    console.log( 'uploadResult', uploadResult );
    if ( uploadResult.status !== 200 ) {
      alert( 'Failed' );
    } else {
      alert( 'File Upload Success' )
    }

    setBusy(false);
  }

  const onFileChange = event => {
    setFilelist( event.target.files );
  }

  return (
    <Form loading={busy}>
      {selfAssessmentFields.map( field => <RadioGroup key={field.name} field={field.name} label={field.label} /> )}
      <Button as="label" htmlFor="file" type="button">
        Upload a file
      </Button>
      <input type="file" id="file" style={{ display: "hidden" }} onChange={onFileChange} />
      <br /><br />
      <Button primary onClick={onSubmitClicked}>Submit</Button>
    </Form>
  )
}

function RadioGroup( props ) {
  const { field, label } = props;

  return (
    <Form.Group inline>
      <label>{label}</label>
      <Form.Field value="Low" label="Low" control="input" type="radio" name={field} />
      <Form.Field value="Medium" label="Medium" control="input" type="radio" name={field} />
      <Form.Field value="High" label="High" control="input" type="radio" name={field} />
    </Form.Group>
  );
}
