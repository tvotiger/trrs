
import React, { useState } from 'react';
import axios from 'axios';
import { Form, Button, Container, Header } from 'semantic-ui-react';
import {useMap} from 'react-use';

export default function CreateProjectForm() {

  const [ busy, setBusy ] = useState(false);

  const [map, {set}] = useMap({
    name        : '',
    description : '',
  });

  const onSubmitClicked = async () => {
    setBusy(true);

    const result = await axios.post(
      `http://108.61.23.159/projects`,
      map,
      { headers : { 'Content-Type': 'application/json' } }
    )
    console.log( 'result', result );
    if ( result.status !== 200 ) {
      alert( 'Failed' );
      setBusy(false);
      return;
    } else {
      alert( 'Success' );
    }

    setBusy(false);
  }

  return (
    <Container text style={{ marginTop: '7em' }}>
      <Header as='h1'>Add a New Project</Header>
      <Form loading={busy}>
        <Form.Field>
          <label>Project Name</label>
          <input type="text" value={map.name} onChange={e => { set('name', e.target.value ) }} />
        </Form.Field>
        <Form.Field>
          <label>Description</label>
          <input type="text" placeholder="Lorem ipsum" value={map.description}
            onChange={e => { set('description', e.target.value ) }} />
        </Form.Field>
        <br />
        <Button primary onClick={onSubmitClicked}>Submit</Button>
      </Form>
    </Container>
  )
}
