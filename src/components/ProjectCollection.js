import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Grid } from 'semantic-ui-react';
import ProjectCard from './ProjectCard.js';

export default function ProjectCollection() {
  const [ projects, setProjects ] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get('http://108.61.23.159/projects');
      setProjects(result.data);
    }
    fetchData();
  }, []);

  console.log( 'projs', projects );

  const cards = projects.map( p => {
    return <Grid.Column key={p.id}><ProjectCard {...p} /></Grid.Column>
  } );

  return (
    <Grid>
      <Grid.Row columns={3}>
        {cards}
      </Grid.Row>
    </Grid>
  );
}